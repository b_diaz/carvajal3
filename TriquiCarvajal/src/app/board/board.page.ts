import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-board',
  templateUrl: './board.page.html',
  styleUrls: ['./board.page.scss'],
})
export class BoardPage implements OnInit {

  squares: string[];
  xIsNext: boolean;
  winner: string;
  linesWin: number[];

  constructor(public alertController: AlertController) { }

  ngOnInit() {
    this.newGame();
  }

  newGame() {
    this.squares = Array(9).fill(null);
    this.winner = null;
    this.xIsNext = true;
    this.linesWin = Array(9).fill(null);
  }

  get player() {
    return this.xIsNext ? 'X' : 'O';
  }

  async makeMove(idx: number) {
    console.log("condicion", !this.squares[idx]);
    console.log("Ingresa indice", idx);


    if (!this.squares[idx]) {
      // this.squares.splice(idx, 1, this.player);
      this.squares[idx] = this.player;     

      this.xIsNext = !this.xIsNext;
    }

    this.winner = await this.calculateWinner();
  }

  async calculateWinner() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (this.squares[a] && this.squares[a] === this.squares[b] && this.squares[a] === this.squares[c]
      ) {
        console.log("Imprime los valores de los valores del triqui", lines[i]);
        this.linesWin = lines[i];        
        await this.presentAlert("El Jugador " + this.squares[a] + " Gana");
        return this.squares[a];
      }
    }
    if(!this.squares.includes(null)){
      await this.presentAlert("Empate");
    }
    return null;
  }

  async presentAlert(title: string) {
    const alert = await this.alertController.create({
      header: title,
      backdropDismiss: false,
      message: 'Gracias por Jugar',
      buttons: [ {
          text: 'Ok',
          handler: () => {
            this.newGame();
          }
        }
      ]
    });

    await alert.present();
  }

}
