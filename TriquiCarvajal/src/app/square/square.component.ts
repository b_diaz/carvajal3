import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss'],
})
export class SquareComponent implements OnInit {

  @Input() value: 'X' | 'O';
  @Input() win: boolean;

  constructor() { }

  ngOnInit() {
    console.log("Se inicia square");
    if (this.win) {
      console.log("Aplicar clase");
    }
  }


}
